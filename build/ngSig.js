﻿'use strict';

var sigServices = angular.module('sigServices', []);


sigServices.service('fbService', [
    function () {

        // Check login status and subscribe if the user is not connected
        this.checkLoginStatus = function (callback) {
            FB.getLoginStatus(function (response) {                
                if (response.status == 'connected') {
                    callback(response);
                } else {
                    FB.Event.subscribe('auth.login', callback);
                }
            });
        };
        this.login = function () {              
            FB.login(function () {}, { scope: 'public_profile,email,user_friends' });
        };
        // Get the user information from FB
        this.getUserInfo = function (callback) {
            FB.api('/me', callback);
        };
        this.invite = function (message, callback) {
            FB.ui({
                method: 'apprequests',
                message: message
            }, callback);
        };
        this.shareApp = function (appUrl, callback) {
            FB.ui({
                method: 'share',
                href: appUrl
            }, callback);
        };
    }
]);

sigServices.service('igService', ['$http', 'stringService',
    function ($http, stringService) {

        var popup = function (url, title, w, h) {
          var left = (screen.width/2)-(w/2);
          var top = (screen.height/2)-(h/2);
          return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
        };

        this.login = function () {
            popup(stringService.format('https://instagram.com/oauth/authorize/?client_id={0}&redirect_uri={1}&response_type=token', igAppId, igRedirectUrl), 'authWindow', 500, 400);            
        };

        this.getUserInfo = function (token, callback) {            
            $http({
                method: 'JSONP',
                url: stringService.format('https://api.instagram.com/v1/users/self?callback=JSON_CALLBACK&access_token={0}', token)
            }).success(function (response) {
                callback(response);
            });
        };
    }
]);

sigServices.service('stringService', function () {

    // Works like String.format() of C#
    this.format = function (text) {
        var args = Array.prototype.slice.call(arguments, 1);
        return text.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
              ? args[number]
              : match
            ;
        });
    };
});

sigServices.service('nopService', ['$rootScope', '$http', 'stringService',
    function ($rootScope, $http, stringService) {
        var authenticate = function (url, accessToken, callback) {
            $rootScope.loadingAjax++;

            $http({
                method: 'POST',
                url: url,
                data: { accessToken: accessToken }
            }).success(function (response) {
                if (response == '200') {
                    console.log('Logged in Nop');
                    callback(true);
                } else {
                    alert('Unable to login with Nop');
                    console.log('Unable to login with Nop');
                    callback(false);
                }
                $rootScope.loadingAjax--;
            });    
        };

        // Login in Nop Commerce using Facebook
        this.authenticateWithFb = function (accessToken, callback) {        
            authenticate('/sig/signInWithFb', accessToken, callback);  
        };

        // Login in Nop Commerce using Instagram
        this.authenticateWithIg = function (accessToken, callback) {        
            authenticate('/sig/signInWithIg', accessToken, callback);  
        };

        this.getRewardPoints = function (callback) {
            $rootScope.loadingAjax++;

            $http({
                method: 'POST',
                url: '/getPointsBalance'
            }).success(function (response) {
                callback(response);
                $rootScope.loadingAjax--;
            });
        };

        this.getTransactionsList = function (callback) {
            $rootScope.loadingAjax++;

            $http({
                method: 'GET',
                url: '/transactionsList'
            }).success(function (response) {
                callback(response);
                $rootScope.loadingAjax--;
            });
        };

        this.getProductsList = function (callback) {
            $rootScope.loadingAjax++;

            $http({
                method: 'GET',
                url: '/socialProductsList'
            }).success(function (response) {
                callback(response);
                $rootScope.loadingAjax--;
            });
        };

        this.addToCart = function (id, quantity, callback) {
            $rootScope.loadingAjax++;

            $http({
                method: 'POST',
                url: stringService.format('/addProductToCartER/catalog/{0}/{1}', id, quantity)
            }).success(function (response) {
                callback(response);
                $rootScope.loadingAjax--;
            });
        };

        // Get the quantity of differents items in the cart (doesn't count amount of each item)
        this.getQuantityInCart = function (callback) {
            $rootScope.loadingAjax++;

            $http({
                method: 'GET',
                url: '/cartItems'
            }).success(function (response) {
                callback(parseInt(response));
                $rootScope.loadingAjax--;
            });
        };

        this.invite = function (friends, message, callback) {
            var data = {
                friendsList: friends,
                message: message
            };
            $rootScope.loadingAjax++;

            $http({
                method: 'POST',
                url: '/customer/inviteFriends',
                data: data
            }).success(function (response) {
                callback(response);
                $rootScope.loadingAjax--;
            });
        };

        this.getBadges = function (callback) {
            $rootScope.loadingAjax++;

            $http({
                method: 'GET',
                url: '/customer/customerBadge'
            }).success(function (response) {
                callback(response);
                $rootScope.loadingAjax--;
            });
        };
    }
]);
var SigService = function ($rootScope, $http, stringService, userUrl, linkUrl, registerInLoyaltyUrl, getTransactionsUrl, getRewardPointsUrl, getRewardDetailsUrl, inviteUrl, shareAppUrl) {    
    
    // Get user registration status
    this.getUser = function (token, appId, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'GET',
            url: stringService.format(userUrl, token, appId)
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };
    
    // Register in SIG and Loyalty
    this.register = function (token, appId, data, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'POST',
            data: data,
            url: stringService.format(userUrl, token, appId)
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };

    // Edit profile in SIG and Loyalty
    this.editProfile = function (token, appId, data, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'PUT',
            data: data,
            url: stringService.format(userUrl, token, appId)
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };

    // Link another account with the current one
    this.linkWithOtherAccount = function (fbToken, fbAppId, igToken, igAppId, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'POST',
            url: stringService.format(linkUrl, fbToken, fbAppId, igToken, igAppId)
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };

    // Register in Loyalty (only when registration failed before)
    this.registerInLoyalty = function (token, appId, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'POST',
            url: stringService.format(registerInLoyaltyUrl, token, appId)
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };

    // Get reward details
    this.getRewardDetails = function (token, appId, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'GET',
            url: stringService.format(getRewardDetailsUrl, token, appId)
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };

    // Invite a friend
    this.invite = function (token, appId, friends, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'POST',
            url: stringService.format(inviteUrl, token, appId),
            data: { invite: friends }
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };

    // Share the App
    this.shareApp = function (token, appId, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'POST',
            url: stringService.format(shareAppUrl, token, appId)
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };
};

sigServices.factory('sigServiceForFb', ['$rootScope', '$http', 'stringService',
    function ($rootScope, $http, stringService) {
        return new SigService(
            $rootScope,
            $http,
            stringService,
            baseApiUrl + 'api/facebook/user?fb_token={0}&fb_app_id={1}',
            baseApiUrl + 'api/facebook/user?fb_token={0}&fb_app_id={1}&ig_token={2}&ig_app_id={3}',
            baseApiUrl + 'api/facebook/user/LoyaltyPlatform?fb_token={0}&fb_app_id={1}',
            baseApiUrl + 'api/facebook/user/transactions?fb_token={0}&fb_app_id={1}&next_cursor={2}',
            baseApiUrl + 'api/facebook/user/rewards?fb_token={0}&fb_app_id={1}',
            baseApiUrl + 'api/facebook/user/rewards/detailed?fb_token={0}&fb_app_id={1}',
            baseApiUrl + 'api/facebook/invite?fb_token={0}&fb_app_id={1}',
            baseApiUrl + 'api/facebook/user/shareapp?fb_token={0}&fb_app_id={1}'
        );
    }
]);


sigServices.factory('sigServiceForIg', ['$rootScope', '$http', 'stringService',
    function ($rootScope, $http, stringService) {
        return new SigService(
            $rootScope,
            $http,
            stringService,
            baseApiUrl + 'api/instagram/user?ig_token={0}&ig_app_id={1}',
            baseApiUrl + 'api/instagram/user?fb_token={0}&fb_app_id={1}&ig_token={2}&ig_app_id={3}',
            baseApiUrl + 'api/instagram/user/LoyaltyPlatform?ig_token={0}&ig_app_id={1}',
            baseApiUrl + 'api/instagram/user/transactions?ig_token={0}&ig_app_id={1}&next_cursor={2}',
            baseApiUrl + 'api/instagram/user/rewards/detailed?ig_token={0}&ig_app_id={1}',
            baseApiUrl + 'api/instagram/user/rewards?ig_token={0}&ig_app_id={1}'
        );
    }
]);
