# ngSig: A library to use SIG and Social Networks APIs with Angular

There are two importants files: __ngSig.js__ and __sigHelpersServices.js__

## ngSig.js

Can be find in build/ folder. It's a bundle of the following files located in the src/ folder:
* socialServices.js
* nopServices.js
* sigServices.js

To use it simply add the string 'sigServices' to the array of the modules of your angular app.


## sigHelpersServices.js

Can be find in src/ folder. It's used by the projects for CSB Engage, and is a particular implementation of using ngSig.js.
To use it you have to:

* Include moment.js
* Configure fbSettings and igSettings values. For example:

```javascript
sigHelpersServices.value('fbSettings', {
    appUrl: '//www.facebook.com/csbengage',
    appId: '629671523753989',
    token: null
});

sigHelpersServices.value('igSettings', {
    appUrl: '//www.instagram.com/csbengage',
    appId: '591542d33d9e4c6fb420a251e7aac7aa',
    token: null
});

```

* Have, at least, this routes defined:

```javascript
app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
        when('/userPanel', {
            templateUrl: contentUrl + 'partials/userPanel.html',
            controller: 'userPanelController'
        }).
        when('/register', {
            templateUrl: contentUrl + 'partials/register.html',
            controller: 'registerController'
        }).
        when('/', {
            templateUrl: contentUrl + 'partials/preLogin.html'
        }).
        otherwise({
            redirectTo: '/'
        });
    }
]);

```

* Have the following variables in $rootScope:
```javascript
$rootScope.status = {
    registeredOnSigWithFb: false,
    registeredOnSigWithIg: false,           
    loggedInWithFb: false,
    loggedInWithIg: false,
    loggedInNop: false,
    userInfoReady: false, // Used to retrieve the user information only once (not one time per every social network)
    socialNetworkReadyCount: 0, // Used as a semaphore to know when the app has finished checking the login status for every social network         
    registrationCompleted: false // Used to display a notification after user finish registration (only once)
};
$rootScope.TOTAL_SOCIAL_NETWORKS = 2;

$rootScope.loadingAjax = 0; // An integer is used to handle multiple ajax requests at the same time         

$rootScope.user = {};

```

* Implement the LoadUserService. For example:

```javascript
var LoadUserService = function ($rootScope, dateService, stringService) {

    this.fromFb = function (response) {
        var user = $rootScope.user;
        
        user.profile_picture = stringService.format('//graph.facebook.com/{0}/picture?width=125&height=125', response.id);
        user.first_name = response.first_name;
        user.last_name = response.last_name;
        user.email = response.email;
        
        if (response.gender)
            user.gender = response.gender.charAt(0).toUpperCase() + response.gender.slice(1);

        // Fix date format (from mm/dd/yyyy to dd/mm/yyyy)                                            
        if (response.birthday)                        
            user.birthday = dateService.MDYtoDMY(response.birthday);
    };

    this.fromIg = function (response) {
        var user = $rootScope.user;
        
        user.profile_picture = response.data.profile_picture;
    };

    this.fromSIG = function (response) {
        var user = $rootScope.user;

        response.data.socialNetworkAccounts.forEach(function (account) {
            if (account.socialNetworkName == 'Facebook') {
                $rootScope.status.registeredOnSigWithFb = true;
            } else if (account.socialNetworkName == 'Instagram') {
                $rootScope.status.registeredOnSigWithIg = true;
            }
        });
        
        user.id = response.data.socialNetworkObjectId;
        user.first_name = response.data.firstName;
        user.last_name = response.data.lastName;
        user.birthday = dateService.unixToDMY(response.data.dateOfBirth);
        user.gender = response.data.gender;
        user.email = response.data.email;
        user.mobile_number = response.data.mobile;
    };
};

```