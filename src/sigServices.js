
var SigService = function ($rootScope, $http, stringService, userUrl, linkUrl, registerInLoyaltyUrl, getTransactionsUrl, getRewardPointsUrl, getRewardDetailsUrl, inviteUrl, shareAppUrl) {    
    
    // Get user registration status
    this.getUser = function (token, appId, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'GET',
            url: stringService.format(userUrl, token, appId)
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };
    
    // Register in SIG and Loyalty
    this.register = function (token, appId, data, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'POST',
            data: data,
            url: stringService.format(userUrl, token, appId)
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };

    // Edit profile in SIG and Loyalty
    this.editProfile = function (token, appId, data, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'PUT',
            data: data,
            url: stringService.format(userUrl, token, appId)
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };

    // Link another account with the current one
    this.linkWithOtherAccount = function (fbToken, fbAppId, igToken, igAppId, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'POST',
            url: stringService.format(linkUrl, fbToken, fbAppId, igToken, igAppId)
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };

    // Register in Loyalty (only when registration failed before)
    this.registerInLoyalty = function (token, appId, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'POST',
            url: stringService.format(registerInLoyaltyUrl, token, appId)
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };

    // Get reward details
    this.getRewardDetails = function (token, appId, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'GET',
            url: stringService.format(getRewardDetailsUrl, token, appId)
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };

    // Invite a friend
    this.invite = function (token, appId, friends, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'POST',
            url: stringService.format(inviteUrl, token, appId),
            data: { invite: friends }
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };

    // Share the App
    this.shareApp = function (token, appId, callback) {
        $rootScope.loadingAjax++;

        $http({
            method: 'POST',
            url: stringService.format(shareAppUrl, token, appId)
        }).success(function (response) {
            callback(response);
            $rootScope.loadingAjax--;
        });
    };
};

sigServices.factory('sigServiceForFb', ['$rootScope', '$http', 'stringService',
    function ($rootScope, $http, stringService) {
        return new SigService(
            $rootScope,
            $http,
            stringService,
            baseApiUrl + 'api/facebook/user?fb_token={0}&fb_app_id={1}',
            baseApiUrl + 'api/facebook/user?fb_token={0}&fb_app_id={1}&ig_token={2}&ig_app_id={3}',
            baseApiUrl + 'api/facebook/user/LoyaltyPlatform?fb_token={0}&fb_app_id={1}',
            baseApiUrl + 'api/facebook/user/transactions?fb_token={0}&fb_app_id={1}&next_cursor={2}',
            baseApiUrl + 'api/facebook/user/rewards?fb_token={0}&fb_app_id={1}',
            baseApiUrl + 'api/facebook/user/rewards/detailed?fb_token={0}&fb_app_id={1}',
            baseApiUrl + 'api/facebook/invite?fb_token={0}&fb_app_id={1}',
            baseApiUrl + 'api/facebook/user/shareapp?fb_token={0}&fb_app_id={1}'
        );
    }
]);


sigServices.factory('sigServiceForIg', ['$rootScope', '$http', 'stringService',
    function ($rootScope, $http, stringService) {
        return new SigService(
            $rootScope,
            $http,
            stringService,
            baseApiUrl + 'api/instagram/user?ig_token={0}&ig_app_id={1}',
            baseApiUrl + 'api/instagram/user?fb_token={0}&fb_app_id={1}&ig_token={2}&ig_app_id={3}',
            baseApiUrl + 'api/instagram/user/LoyaltyPlatform?ig_token={0}&ig_app_id={1}',
            baseApiUrl + 'api/instagram/user/transactions?ig_token={0}&ig_app_id={1}&next_cursor={2}',
            baseApiUrl + 'api/instagram/user/rewards/detailed?ig_token={0}&ig_app_id={1}',
            baseApiUrl + 'api/instagram/user/rewards?ig_token={0}&ig_app_id={1}'
        );
    }
]);
