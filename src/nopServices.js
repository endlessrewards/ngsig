
sigServices.service('nopService', ['$rootScope', '$http', 'stringService',
    function ($rootScope, $http, stringService) {
        var authenticate = function (url, accessToken, callback) {
            $rootScope.loadingAjax++;

            $http({
                method: 'POST',
                url: url,
                data: { accessToken: accessToken }
            }).success(function (response) {
                if (response == '200') {
                    console.log('Logged in Nop');
                    callback(true);
                } else {
                    alert('Unable to login with Nop');
                    console.log('Unable to login with Nop');
                    callback(false);
                }
                $rootScope.loadingAjax--;
            });    
        };

        // Login in Nop Commerce using Facebook
        this.authenticateWithFb = function (accessToken, callback) {        
            authenticate('/sig/signInWithFb', accessToken, callback);  
        };

        // Login in Nop Commerce using Instagram
        this.authenticateWithIg = function (accessToken, callback) {        
            authenticate('/sig/signInWithIg', accessToken, callback);  
        };

        this.getRewardPoints = function (callback) {
            $rootScope.loadingAjax++;

            $http({
                method: 'POST',
                url: '/getPointsBalance'
            }).success(function (response) {
                callback(response);
                $rootScope.loadingAjax--;
            });
        };

        this.getTransactionsList = function (callback) {
            $rootScope.loadingAjax++;

            $http({
                method: 'GET',
                url: '/transactionsList'
            }).success(function (response) {
                callback(response);
                $rootScope.loadingAjax--;
            });
        };

        this.getProductsList = function (callback) {
            $rootScope.loadingAjax++;

            $http({
                method: 'GET',
                url: '/socialProductsList'
            }).success(function (response) {
                callback(response);
                $rootScope.loadingAjax--;
            });
        };

        this.addToCart = function (id, quantity, callback) {
            $rootScope.loadingAjax++;

            $http({
                method: 'POST',
                url: stringService.format('/addProductToCartER/catalog/{0}/{1}', id, quantity)
            }).success(function (response) {
                callback(response);
                $rootScope.loadingAjax--;
            });
        };

        // Get the quantity of differents items in the cart (doesn't count amount of each item)
        this.getQuantityInCart = function (callback) {
            $rootScope.loadingAjax++;

            $http({
                method: 'GET',
                url: '/cartItems'
            }).success(function (response) {
                callback(parseInt(response));
                $rootScope.loadingAjax--;
            });
        };

        this.invite = function (friends, message, callback) {
            var data = {
                friendsList: friends,
                message: message
            };
            $rootScope.loadingAjax++;

            $http({
                method: 'POST',
                url: '/customer/inviteFriends',
                data: data
            }).success(function (response) {
                callback(response);
                $rootScope.loadingAjax--;
            });
        };

        this.getBadges = function (callback) {
            $rootScope.loadingAjax++;

            $http({
                method: 'GET',
                url: '/customer/customerBadge'
            }).success(function (response) {
                callback(response);
                $rootScope.loadingAjax--;
            });
        };
    }
]);