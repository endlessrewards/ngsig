﻿'use strict';

var sigServices = angular.module('sigServices', []);


sigServices.service('fbService', [
    function () {

        // Check login status and subscribe if the user is not connected
        this.checkLoginStatus = function (callback) {
            FB.getLoginStatus(function (response) {                
                if (response.status == 'connected') {
                    callback(response);
                } else {
                    FB.Event.subscribe('auth.login', callback);
                }
            });
        };
        this.login = function () {              
            FB.login(function () {}, { scope: 'public_profile,email,user_friends' });
        };
        // Get the user information from FB
        this.getUserInfo = function (callback) {
            FB.api('/me', callback);
        };
        this.invite = function (message, callback) {
            FB.ui({
                method: 'apprequests',
                message: message
            }, callback);
        };
        this.shareApp = function (appUrl, callback) {
            FB.ui({
                method: 'share',
                href: appUrl
            }, callback);
        };
    }
]);

sigServices.service('igService', ['$http', 'stringService',
    function ($http, stringService) {

        var popup = function (url, title, w, h) {
          var left = (screen.width/2)-(w/2);
          var top = (screen.height/2)-(h/2);
          return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
        };

        this.login = function () {
            popup(stringService.format('https://instagram.com/oauth/authorize/?client_id={0}&redirect_uri={1}&response_type=token', igAppId, igRedirectUrl), 'authWindow', 500, 400);            
        };

        this.getUserInfo = function (token, callback) {            
            $http({
                method: 'JSONP',
                url: stringService.format('https://api.instagram.com/v1/users/self?callback=JSON_CALLBACK&access_token={0}', token)
            }).success(function (response) {
                callback(response);
            });
        };
    }
]);

sigServices.service('stringService', function () {

    // Works like String.format() of C#
    this.format = function (text) {
        var args = Array.prototype.slice.call(arguments, 1);
        return text.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
              ? args[number]
              : match
            ;
        });
    };
});
