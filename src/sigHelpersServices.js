'use strict';

var sigHelpersServices = angular.module('sigHelpersServices', []);


sigHelpersServices.value('fbSettings', {
	appUrl: null,
	appId: null,
	token: null
});

sigHelpersServices.value('igSettings', {
	appUrl: null,
	appId: null,
	token: null
});

var LoadUserService = function ($rootScope, dateService, stringService) {

	this.fromFb = function (response) {	};

	this.fromIg = function (response) {	};

	this.fromSIG = function (response) { };
};

sigHelpersServices.factory('loadUserService', ['$rootScope', 'dateService', 'stringService',
    function ($rootScope, dateService, stringService) {
        return new LoadUserService($rootScope, dateService, stringService);
    }
]);


sigHelpersServices.service('sigHelpersForFb', ['$rootScope', '$window', '$timeout', '$location', '$route', 'sigServiceForFb', 'fbService', 'fbSettings', 'loadUserService', 'nopService',
	function ($rootScope, $window, $timeout, $location, $route, sigServiceForFb, fbService, fbSettings, loadUserService, nopService) {

		var incrementSocialNetworkCounter = function () {
			$timeout(function () { // Force the $apply() to rootScope after changing the value
				$rootScope.status.socialNetworkReadyCount++; 
			});
		};

		this.init = function () {
			var self = this;
			$window.fbAsyncInit = function () {
				FB.init({
					appId: fbAppId,
					status: true, // check login status
					cookie: true, // enable cookies to allow the server to access the session
					xfbml: true,  // parse XFBML
					version: 'v2.2'
				});

				fbService.checkLoginStatus(self.getUserStatusCallback);			
			};
			// Load the FB SDK asynchronously
			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = '//connect.facebook.net/en_US/sdk.js';
				fjs.parentNode.insertBefore(js, fjs);
	       }(document, 'script', 'facebook-jssdk'));
		};

		this.getUserStatusCallback = function (response) {
			if (response.status == 'connected') { //if user is logged on to FB then get additional info and then check SIG status
				$rootScope.status.loggedInWithFb = true;
				fbSettings.token = response.authResponse.accessToken;

				if (!$rootScope.status.userInfoReady) {
					$rootScope.status.userInfoReady = true;       		
					fbService.getUserInfo(function (response) {					
	                    loadUserService.fromFb(response);
					});
					
					sigServiceForFb.getUser(fbSettings.token, fbSettings.appId, function (response) {
						switch (response.code) {
							case 2: // Not registered yet
								$rootScope.status.registeredOnSigWithFb = false;
								break;
							case 4: // Have to register in Loyalty
								//disable the below call until the loyalty platform integration gets fixed
								//sigServiceForFb.registerInLoyalty(fbSettings.token, fbSettings.appId, function (response) { });
								console.log('FB: The user is registered in SIG only');
								loadUserService.fromSIG(response);
								$rootScope.status.registeredOnSigWithFb = true;
								break;
							case 6:
								console.log('FB: The user is registered in SIG and Loyalty');
								loadUserService.fromSIG(response);
								$rootScope.status.registeredOnSigWithFb = true;								
								break;
						}
						incrementSocialNetworkCounter();
					});
				} else {
					incrementSocialNetworkCounter();
				}

			} else { // User is not logged in on facebook
				$rootScope.status.loggedInWithFb = false;
				$rootScope.status.registeredOnSigWithFb = false;
				incrementSocialNetworkCounter();
			}
		};

		this.login = function () {			
			if ($rootScope.status.loggedInWithFb) {
				$location.path('/register');
				$route.reload();
			} else
				fbService.login();
		};

		this.register = function (data) {			
            sigServiceForFb.register(fbSettings.token, fbSettings.appId, data, function (response) {
                if (response.code == 4 || response.code == 6) {                    
                    nopService.authenticateWithFb(fbSettings.token, function () {
                    	$rootScope.status.registeredOnSigWithFb = true;
                    	$rootScope.status.registrationCompleted = true;
                    	$location.path('/userPanel');
                    });
                }
            });
		};

		this.editProfile = function (data, callback) {			
            sigServiceForFb.editProfile(fbSettings.token, fbSettings.appId, data, function (response) {
                if (response.code == 9 || response.code == 10 || response.code == 11) {
                   callback(response.data);
                }
            });
		};

		this.invite = function (message, callback) {
			fbService.invite(message, function (response) {
				sigServiceForFb.invite(fbSettings.token, fbSettings.appId, response.to, callback);
			});		        
		};


		this.shareApp = function (appUrl, callback) {
			fbService.shareApp(appUrl, function (response) {
				if (response instanceof Array) // We check this way because Facebook API is returning [] if the post was shared
					sigServiceForFb.shareApp(fbSettings.token, fbSettings.appId, callback);
				else
					callback(null);
			});
		};
    }
]);

function onInstagramAuth (accessToken) {
	var injector = angular.element('html').injector();
	injector.get('sigHelpersForIg').getUserStatusCallback(accessToken);
}

sigHelpersServices.service('sigHelpersForIg', ['$rootScope', '$timeout', '$location', '$route', 'sigServiceForIg', 'igService', 'igSettings', 'loadUserService', 'nopService',
	function ($rootScope, $timeout, $location, $route, sigServiceForIg, igService, igSettings, loadUserService, nopService) {

		var incrementSocialNetworkCounter = function () {
			$timeout(function () { // Force the $apply() to rootScope after changing the value
				$rootScope.status.socialNetworkReadyCount++; 
			});
		};

		this.init = function () {
			// User is not logged in on instagram until they press the button
			$rootScope.status.loggedInWithIg = false;
			$rootScope.status.registeredOnSigWithIg = false;
			incrementSocialNetworkCounter();
		};

		this.getUserStatusCallback = function (accessToken) {
			$rootScope.status.loggedInWithIg = true;
			igSettings.token = accessToken;

			if (!$rootScope.status.userInfoReady) {
				$rootScope.status.userInfoReady = true;
				igService.getUserInfo(igSettings.token, function (response) {					
		            loadUserService.fromIg(response);
				});

				sigServiceForIg.getUser(igSettings.token, igSettings.appId, function (response) {
					switch (response.code) {
						case 2: // Not registered yet
							$rootScope.status.registeredOnSigWithIg = false;						
							break;
						case 4: // Have to register in Loyalty
							//disable the below call until the loyalty platform integration gets fixed
							//sigServiceForIg.registerInLoyalty(igSettings.token, igSettings.appId, function (response) { });
							console.log('IG: The user is registered in SIG only');
							loadUserService.fromSIG(response);                    
							$rootScope.status.registeredOnSigWithIg = true;							
							break;
						case 6:
							console.log('IG: The user is registered in SIG and Loyalty');
							loadUserService.fromSIG(response);
							$rootScope.status.registeredOnSigWithIg = true;							
							break;
					}
					incrementSocialNetworkCounter();
				});
			} else {
				incrementSocialNetworkCounter();
			}
		};

		this.login = function () {
			if ($rootScope.status.loggedInWithIg) {
				$location.path('/register');
				$route.reload();
			} else
				igService.login();
		};

		this.register = function (data) {			
            sigServiceForIg.register(igSettings.token, igSettings.appId, data, function (response) {
                if (response.code == 4 || response.code == 6) {
                    nopService.authenticateWithIg(igSettings.token, function () {
						$rootScope.status.registeredOnSigWithIg = true;
                    	$rootScope.status.registrationCompleted = true;
                    	$location.path('/userPanel');
                    });
                }
            });
		};

		this.editProfile = function (data, callback) {			
            sigServiceForIg.editProfile(igSettings.token, igSettings.appId, data, function (response) {
                if (response.code == 9 || response.code == 10 || response.code == 11) {
                    callback(response.data);
                }
            });
		};
    }
]);




sigHelpersServices.service('sigHelpersForRouting', ['$rootScope', '$location', '$route', 'sigServiceForFb', 'sigServiceForIg', 'nopService', 'fbSettings', 'igSettings',
	function ($rootScope, $location, $route, sigServiceForFb, sigServiceForIg, nopService, fbSettings, igSettings) {

		this.redirect = function () {
			$location.path('/'); // Redirect to Home page and then decide where to send the user

			$rootScope.$watch('status.socialNetworkReadyCount', function() {
				var status = $rootScope.status;

				if (status.socialNetworkReadyCount < $rootScope.TOTAL_SOCIAL_NETWORKS)
					return;
				
				console.log('Logged with FB: ' + status.loggedInWithFb);
				console.log('Logged with IG: ' + status.loggedInWithIg);
				console.log('Registered with FB: ' + status.registeredOnSigWithFb);
				console.log('Registered with IG: ' + status.registeredOnSigWithIg);

				if (status.registeredOnSigWithIg || status.registeredOnSigWithFb) { // If registered on SIG

					if (status.loggedInWithIg && !status.registeredOnSigWithIg) { // The user is registered and logged with Ig but not linked yet
						sigServiceForIg.linkWithOtherAccount(fbSettings.token, fbSettings.appId, igSettings.token, igSettings.appId, function(response) {							
							status.registeredOnSigWithIg = true;
							$location.path('/userPanel');
							$route.reload();
						});
					}
					else if (status.loggedInWithFb && !status.registeredOnSigWithFb) { // The user is registered and logged with Fb but not linked yet
						sigServiceForFb.linkWithOtherAccount(fbSettings.token, fbSettings.appId, igSettings.token, igSettings.appId, function(response) {
							status.registeredOnSigWithFb = true;
							$location.path('/userPanel');
							$route.reload();
						});
					} 
					else { // The user is not willing to link another account

						if (status.loggedInNop) {
							$location.path('/userPanel');
						} else {
							var nopCallback = function (isAuth) {
					            if (isAuth) {				                
					                status.loggedInNop = true; // Logged in Nop
					                $location.path('/userPanel');
					            }
					        };

							if (fbSettings.token)
				            	nopService.authenticateWithFb(fbSettings.token, nopCallback);
				        	else if (igSettings.token)
								nopService.authenticateWithIg(igSettings.token, nopCallback);							
						}
					}						
				} 
				else if (status.loggedInWithIg || status.loggedInWithFb) { // Not registered in SIG
					$location.path('/register');											
				}				
			});
		};
	}
]);


sigHelpersServices.service('dateService', [
    function () {
    	// Transform dd/mm/yyyy to unix time
        this.DMYtoUnix = function (dateString) {            
            return moment.utc(dateString, 'D/M/YYYY').format('X'); // Use utc() to ignore timezone
        };

        this.unixToDMY = function (dateString) {            
            return moment(dateString, 'X').format('D/M/YYYY');
        };

        this.MDYtoDMY = function (dateString) {   
        	return moment(dateString, 'M/D/YYYY').format('D/M/YYYY');
        };

        this.getAge = function (DMY) {
        	var birthday = moment(DMY, 'DD/MM/YYYY');
        	var today = moment();
        	return birthday.fromNow().split(' ')[0] - (birthday.dayOfYear() > today.dayOfYear());
        };
    }
]);